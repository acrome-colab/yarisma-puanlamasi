# Yarışma Puanlama Algoritması

Çok yakın zaman içerisinde ön eleme yarışması olacak ve yarışmadaki puanlamanın nasıl olacağını merak ediyorsunuz. 
Uzun ve detaylı bir çalışmanın sonunda puanlama algoritmasını çıkardık. Aşağıda gerekli açıklamayı bulabilirsiniz.

Öncelikle RIDE Editör’deki metrikleri kısaca açıklamakta fayda var. 
Yarışma metrikleriniz ile ilgili RIDE Editör'ünün alt kısmında lacivert bir bölüm var. 
Bu bölümde yarışma göstergeleri ve yarışma metrikleri gösteriliyor. 

<br> </br>

<img src="Images/RIDE_Metrics_1.png" width="600">

<br> </br>

Öncelikle yarışma göstergelerini inceleyelim:

# Yarışma Göstergeleri

## Disqualification Reason:

*  **Null:** Diskalifiye edilmedi

*  **Incorrect Start:** Robot başlaması gereken noktadan farklı bir noktada başladı

*  **Out of Track:** 1m x 1m’lik karoların dışına çıkıldı

*  **Out of Sequence:** En az 1 parkur karosunun içinden geçilmeden atlandı

*  **Out of Time:** Parkur verilen azami süre içerisinde tamamlanmadı. Bu azami süre deneme ortamında **200**, ön eleme ortamında **100** sn'dir.

Gerçek yarışmada bu durumlardan herhangi birisi gerçekleşirse yarışmadan elenirsiniz. 
Yarışma sonunda algoritmanızı sadece 1 kere çalıştırarak test edeceğimizden dolayı, algoritmanızı çok defa resetleyerek her seferinde çalıştığından emin olun, pistten çıkmalar varsa algoritmanızı mutlaka daha stabil hale getirin.
## **race started:**(True/False)

Simulatör start aldığında veya resetlendiğinde ve robot herhangi bir harekette bulunmadığı sürece race started: False olarak kalır. Robot ilk hareketini gerçekleştirdiği anda race started: True olur.

## **race finished:** (True/False)
Yarışmacı yukarıdaki elenme durumlarını gerçekleştirmeden parkuru tamamlarsa, race finished: True olur.
Parkurun ilk turu dikkate alınmaktadır. race finished : True olduğu anda metrikler durmaktadır, bu da ilk turun sonunda gerçekleşmektedir. 

# Yarışma Metrikleri

## Sum Area (m2): 

Yarışma kurallarında da açıkladığımız üzere sum area, robotun çizgiden ne kadar saptığını gösteren bir metriktir. 
Çizgi boyunca çizgiden sapılan alan sürekli olarak puanlama algoritması tarafından hesaplanmaktadır. 
Sum area ne kadar düşükse sıralamada o kadar yükseleceksiniz. 

<br> </br>

<img src="Images/Yarisma_Kurallari_1.png" width="600">

<br> </br>

## Time (s):

Yarışı ne kadar sürede bitirdiğinizi gösteren metriktir. Yani göstergeler üzerinden konuşacak olursak race started: True ile race finished: True arasındaki geçen zamandır. Haliyle bu süre ne kadar kısa olursa puanınız o kadar yüksek olacaktır. 
Sum Distance (m): Yarış süresince katettiğiniz mesafeyi gösteren bir metriktir.  Aşağıda pistin toplam uzunluğunu sizlerle paylaşıyoruz:

**Toplam Pist Uzunluğu** = 32.9955 metre

Eğer yarış sonucunda aldığınız Sum Distance değeri Toplam Pist Uzunluğu’ndan kısa olursa, puanlama algoritması bitirme sürenize ceza olarak 
kalan mesafe / ortalama hızınız kadar süre ekler ve "Corrected Time" diye yeni bir metrik hesaplar.

<br> </br>

<img src="Images/Corrected_Time_Formula_Final.png" width="600">

<br> </br>

Bu metrik skor hesaplama formülünde kullanılmaktadır.

Aşağıda bir örnek ile açıklayalım:

<br> </br>

<img src="Images/Corrected_Time_F.png" width="800">

<br> </br>

Örnekte görüldüğü üzere, yarışmacı pisti 45 saniyede bitirmesine karşın, daha kısa bir yoldan gittiği için yarışı bitirme süresine bir ceza süresi eklenmiştir ve bitirme süresi 49.493 saniye olmuştur.

## Yarışma Puanlama Formülü

Evet, belki de cevabı en merak edilen sorulardan biri bu yarışma sürecinde yarışma puanının nasıl hesaplanacağıydı. 
Puanlama algoritmamız bu metrikler ışığında puanlamayı nasıl yapıyor?
Yarışma kurallarında temel noktaları belirtmiştik, ancak bazı katsayıları sonra açıklanmak üzere açık bırakmıştık. 
Şimdi puanlama formülünü aşağıda paylaşıyoruz:

Formüle geçmeden dikkat edilmesi gereken çok önemli bir nokta var:

**Ön eleme yarışması ve final yarışması sonunda algoritmanız sadece 1 kere çalıştırılacaktır.**

Algoritmanızın stabil olduğuna mutlaka dikkat edin. 
Birden fazla kere resetleyerek, pistten çıkıp çıkmadığınızı test edin. Eğer pistten çıktığınız oluyorsa, mutlaka algoritmanızı daha stabil hale getirin. 
Bu durum biz algoritmanızı çalıştırdığımızda da başınıza gelebilir ve yarışmadan elenebilirsiniz. 

Aşağıda Yarışma Puanı Hesaplama Formülü'nü paylaşıyoruz.

<br> </br>

<img src="Images/Puanlama_Formulu_Final.png" width="600">

Puanlama algoritmasında temel olarak sum area metriğinizin 0.5 m2'nin altında veya üstünde olma durumuna göre iki farklı katsayı kullanılmaktadır. 
Bu sebeple, sum area metriğinizin 0.5 m2 altında olması önemli bir avantaj sağlamaktadır. Buna dikkat ederek algoritmanızı adapte edebilirsiniz. Sorularınız olursa lütfen Facebook Riders Arena Forumu üzerinden bize ulaşın.

Not: Yarından itibaren RIDE Editör'deki metrikler bölümünde **Score** metriği eklenecektir. Bu metrikler üzerinden algoritmalarınızı optimize edebilirsiniz.

Keyifli bir yarışma olması dileğiyle! Görüşmek üzere!

Riders Ekibi



